json.extract! article, :id, :title, :descrip, :user_created, :user_updated, :created_at, :updated_at
json.url article_url(article, format: :json)
