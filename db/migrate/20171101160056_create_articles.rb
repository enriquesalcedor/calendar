class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.string :title
      t.text :descrip
      t.integer :user_created
      t.integer :user_updated

      t.timestamps
    end
  end
end
